package service;

import java.util.Collections;
import java.util.Map;

public class CountPoint {
    public Integer getVoucher(int point){
        DataService dataService = new DataService();
        dataService.listVoucher();
        var list = dataService.getVoucher();
        Integer max = Collections.max(list.values());
        Integer returnPoint = point - max;
        System.out.println("Anda telah menukar 1000 point dengan voucher terbesar, sisa point anda adalah "
                + returnPoint + " point");
        return returnPoint;
    }
}
