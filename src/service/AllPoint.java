package service;

import java.util.*;

public class AllPoint {
    DataService dataService = new DataService();
    public List<Integer> sorting(){
        var list = dataService.getVoucher();
        List<Integer> sorting = new ArrayList<>(list.values());
        sorting.sort(Collections.reverseOrder());
        return sorting;
    }
    public List<Double> allPoints(int point){
        dataService.listVoucher();
        var list = dataService.getVoucher();
        List<Integer> points = sorting();
        List<Integer> redeem = new ArrayList<>();
        List<Double> voucher = new ArrayList<>();
        for (Integer value : points){
            if (point >= value){
                point = point - value;
                redeem.add(value);
            }
        }
        for (Integer x : redeem) {
            for (Map.Entry<Double, Integer> entry : list.entrySet()){
                if (Objects.equals(entry.getValue(), x)){
                    voucher.add(entry.getKey());
                }
            }
        }return voucher;
    }
    public Integer pointLeft(int point) {
        List<Integer> values = sorting();
        for (Integer value : values) {
            if (point >= value) {
                point = point - value;
            }
        }return point;
    }

}
