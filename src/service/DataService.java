package service;

import java.util.*;

public class DataService {
    private HashMap<Double, Integer> voucher = new HashMap<>();
    public void setVoucher(HashMap<Double, Integer> voucher) {this.voucher = voucher;}
    public void listVoucher() {
        HashMap<Double, Integer> vouchers = new HashMap<>();
        vouchers.put(10000D, 100);
        vouchers.put(25000D, 200);
        vouchers.put(50000D, 400);
        vouchers.put(100000D, 800);
        setVoucher(vouchers);
    }
    public HashMap<Double, Integer> getVoucher() {
        return voucher;
    }
}



