package service;

import java.util.*;

public class BiggestVoucher {

public void biggestVoucher(){
        DataService dataService = new DataService();
        dataService.listVoucher();
        var vouchers = dataService.getVoucher();
        Integer max = Collections.max(vouchers.values());
        for (Map.Entry<Double, Integer> entry : vouchers.entrySet()){
            if (entry.getValue() == max){
                System.out.println("Voucher pulsa terbesar adalah Rp. " + entry.getKey() + " dapat ditukar dengan " + entry.getValue() + " point");
            }
        }
    }
}

