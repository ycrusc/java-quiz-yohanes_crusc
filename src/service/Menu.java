package service;

public class Menu {
    public static void mainMenu () {
        System.out.println("      ");
        System.out.println("                    List Voucher Pulsa   ");
        System.out.println("        * 100 point = Voucher Pulsa Rp.10.000 ");
        System.out.println("        * 200 point = Voucher Pulsa Rp.25.000 ");
        System.out.println("        * 400 point = Voucher Pulsa Rp.50.000 ");
        System.out.println("        * 800 point = Voucher Pulsa Rp.100.000");
        System.out.println("      ");
        System.out.println("        ||=====================================||");
        System.out.println("        ||              LOYALTY POINT          ||");
        System.out.println("        ||=====================================||");
        System.out.println("        ||                 MENU                ||");
        System.out.println("        ||1. Mencari Voucher terbesar          ||");
        System.out.println("        ||2. Tukar 1000 point                  ||");
        System.out.println("        ||3. Tukar semua point                 ||");
        System.out.println("        ||4. Selesai                           ||");
        System.out.println("        ||=====================================||");
        System.out.print("              Pilihan : ");
    }

    public static void PressEnter () {
        System.out.println("Tekan ENTER untuk melanjutkan......");
        try {
            System.in.read();
        } catch (Exception e) {}
    }
}
